import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuardService } from "@src/app/services/auth-guard.service";
import { error } from "util";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./auth/login/login/login.module").then(m => m.LoginModule)
  },
  {
    path: "login",
    loadChildren: () =>
      import("./auth/login/login/login.module").then(m => m.LoginModule)
  },
  {
    path: "home",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/home/home/home.module").then(m => m.HomeModule)
  },
  {
    path: "info",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/info/info/info.module").then(m => m.InfoModule)
  },
  {
    path: "service",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/service-overview/service/service.module").then(
        m => m.ServiceModule
      )
  },
  {
    path: "activities",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/activities/activities/activities.module").then(
        m => m.ActivitiessModule
      )
  },
  {
    path: "places",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/places/places.module").then(m => m.PlacesModule)
  },
  {
    path: "offers",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/offers/offers.module").then(m => m.OffersModule)
  },
  {
    path: "food",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/food/food.module").then(m => m.FoodModule)
  },
  {
    path: "feedback",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/feedback/feedback.module").then(m => m.FeedbackModule)
  },
  {
    path: "map",
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import("./component/Map/map.module").then(m => m.MapModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
