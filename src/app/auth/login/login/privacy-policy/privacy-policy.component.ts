import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '@src/app/services/analytics.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor( private analyticsService : AnalyticsService ) { }

  ngOnInit() {
    this.analyticsService.sendEvent('login-privacy-statement-link-clicked');
  }

}
