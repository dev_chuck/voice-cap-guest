import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '@src/app/auth/login/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PrivacyPolicyComponent } from '@src/app/auth/login/login/privacy-policy/privacy-policy.component';
import { MaterialModule } from 'src/app/material/material.module';
export const LoginRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent }
];


@NgModule({
  declarations: [LoginComponent, PrivacyPolicyComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [CommonModule]
})
export class LoginModule { }
