import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FooterService } from '@src/app/services/footer.service';
import { AnalyticsService } from '@src/app/services/analytics.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private analyticsService : AnalyticsService, private authservice: AuthService, private nav: FooterService) { this.nav.hide() }

  languageSelected = 'en';
  name: any;
  email: any;
  ischecked = false;
  isLoading = true;
  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      name: [''],
      email: [''],
      selectedLanguage: ['']
    });
    if (localStorage.getItem('guestID') != undefined) {
      // this.router.navigate(['/home'])
    } else {
      localStorage.setItem('isLoggedIn', 'false');
      // document.getElementById('viewContainer').style.height = '100%';
      // document.getElementById('bottomnavbar').style.display = 'none';
    }
    if (localStorage.getItem('name') != undefined) {
      this.name = localStorage.getItem('name');
      this.loginForm.patchValue({
        name: this.name
      });
    }
    if (localStorage.getItem('email') != undefined) {
      this.email = localStorage.getItem('email');
      this.loginForm.patchValue({
        email: this.email
      });
    }
    this.isLoading = false;
  }

  postGuest() {
    this.authservice.login(this.loginForm.value).subscribe(
      (data: any) => {
        localStorage.setItem('guestID', data.id);
        console.log('success', data);
      },
      err => {
        console.log(err);
      }
    );
  }

  goToConsentPopUp(skip) {
    this.name = this.loginForm.value.name;
    this.email = this.loginForm.value.email;
    this.languageSelected = this.loginForm.value.selectedLanguage;
    console.log('komt tie hier nu', this.loginForm.value);
    if (!skip) {
      this.analyticsService.sendEvent('login-button-clicked');
    }
    else {
      console.log('komt tie hier nu 1111')
      this.analyticsService.sendEvent('login-skip-link-clicked');

    }
    if (this.languageSelected == '') {
      this.languageSelected = 'en';
    }
    // this.setLanguage(this.languageSelected);
    localStorage.setItem('language', this.languageSelected);
    document.getElementById('guestForm').style.display = 'none';
    document.getElementById('consent').style.display = 'block';
    if (this.name) {
      localStorage.setItem('name', this.name);
      this.analyticsService.sendEvent('login-name-provided');
    }
    if (this.email) {
      localStorage.setItem('email', this.email);
      this.analyticsService.sendEvent('login-email-provided');
    }
    this.analyticsService.sendEvent('login-language-selected-' + this.languageSelected );
  }

  checkConsentCheckBox(event) {
    console.log(event, 'check');
    if (event.target.checked) {
      this.ischecked = true;
      console.log(this.ischecked);
    } else {
      this.ischecked = false;
    }
  }
  giveConsent() {
    this.analyticsService.sendEvent('button-clicked-consent-given');
    localStorage.setItem('isLoggedIn', 'true');
    this.router.navigate(['/home']);
    this.postGuest();
  }
}
