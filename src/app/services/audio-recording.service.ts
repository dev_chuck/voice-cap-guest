import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import * as RecordRTC from "recordrtc";
import * as moment from "moment";
interface RecordedAudioOutput {
  blob: Blob;
  size: number;
  type: string;
}
@Injectable({
  providedIn: "root"
})
export class AudioRecordingService {
  blobSize = 0;
  private stream;
  private recorder;
  private interval;
  private startTime;
  private _recorded = new Subject<RecordedAudioOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<string>();
  abc;
  mediaRecorder;
  numberOfAudioChannels = 2;
  getRecordedBlob(): Observable<RecordedAudioOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }

  startRecording() {
    if (this.recorder) {
      return;
    }

    this._recordingTime.next("00:00");

    navigator.mediaDevices
      .getUserMedia({ audio: true })
      .then(stream => {
        this.stream = stream;
        // let mediaRecorder = new MediaRecorder(stream);
        // this.mediaRecorder.start();

        this.record();
      })
      .catch(error => {
        console.log(error);
        this._recordingFailed.next();
      });
  }

  abortRecording() {
    // alert('test');
    this.stopMedia();
  }

  record() {
    this.recorder = new RecordRTC.StereoAudioRecorder(this.stream, {
      type: "audio",
      mimeType: "audio/webm",
      numberOfAudioChannels: this.numberOfAudioChannels || 2
    });
    //  console.log('this is blob'+ this.getRecordedBlob)

    this.recorder.record();
    console.log("this.recorder.blob");
    console.log(this._recorded);
    // console.log(this.recorder)
    console.log("this is recorder");
    console.log(this.recorder);
    this.startTime = moment();
    this.interval = setInterval(() => {
      const currentTime = moment();
      const diffTime = moment.duration(currentTime.diff(this.startTime));
      const time =
        this.toString(diffTime.minutes()) +
        ":" +
        this.toString(diffTime.seconds());

      console.log("hello bhai");
      // console.log(this.recorder.getInternalRecorder())
      //    var blob =  this._recorded;
      //    console.log(blob)
      //      if(this.blobSize > (100 * 10000)){
      //       this.stopRecording();
      //     }

      //     console.log(a);
      //     this._recorded._subscribe( a => {
      //       this.blobSize = data.blob.size
      //     }
      //  );

      // this._recorded.subscribe( data => {
      //   this.blobSize = data.blob.size ;
      //   console.log(this.blobSize)
      //   if(this.blobSize > (100* 1000)){
      //     this.stopRecording();
      //   }
      // })

      this._recordingTime.next(time);
      // console.log(time)
    }, 1000);
  }
  private toString(value) {
    let val = value;
    if (!value) {
      val = "00";
    }
    if (value < 10) {
      val = "0" + value;
    }
    return val;
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stop(
        blob => {
          console.log("file size");

          console.log(blob);
          if (this.startTime) {
            const mp3Name = encodeURIComponent(
              "audio_" + new Date().getTime() + ".mp3"
            );
            this.stopMedia();
            this._recorded.next(blob);
          }
        },
        () => {
          this.stopMedia();
          this._recordingFailed.next();
        }
      );
    }
  }

  public stopMedia() {
    console.log("mediastop");
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream = null;
      }
    }
  }
}
