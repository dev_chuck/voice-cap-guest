import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  ActivatedRoute,
  CanActivate
} from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Location } from "@angular/common";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAuth = localStorage.getItem("isLoggedIn");
    const a: any = isAuth;
    // console.log(isAuth)
    if (isAuth === null || a === false || isAuth === undefined) {
      console.log("no token");
      this.router.navigate(["/login"]);
      return;
    } else {
      return true;
    }
  }
}
