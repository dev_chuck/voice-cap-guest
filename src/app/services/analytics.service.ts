import { Injectable } from '@angular/core';
declare let gtag: Function;
@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  constructor() {}

  //   public eventEmitter(eventCategory: string,
  //     eventAction: string,
  //     eventLabel: string = null,
  //     eventValue: number = null) {
  //       gtag('send', 'event', {
  // eventCategory: eventCategory,
  // eventLabel: eventLabel,
  // eventAction: eventAction,
  // eventValue: eventValue
  // });
  //     }

  public sendEvent(action) {
    gtag('event', action, {
      action
    });
  }
}

//   function sendEvent(action) {
//     gtag('event', action)
// }

// return{
//     sendEvent: sendEvent
// }
