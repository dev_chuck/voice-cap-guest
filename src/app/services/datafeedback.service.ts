import { Injectable } from '@angular/core';
import { environment } from '@src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DatafeedbackService {
  textFeedback;
  voiceFeedback;
  detectedEntities;
  backfeed;

  constructor(private http: HttpClient, private router: Router) {}

  sendfeedback(data) {
    console.log('feedback_service');

    const url = environment.apiUrl + '/feedback/analyze/ephc/concepts';

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': undefined
      })
    };

    console.log('apis');

    return this.http.post(url, data);

    // .subscribe(data =>
    //   {
    //     console.log(data);
    //     // this.router.navigate(['feedback']);
    //     this.rating(data);

    //  });
  }

  // rating(feed){
  //   alert('rating');
  //   this.backfeed  = feed;
  //     return this.backfeed;
  // }

  getDetectedEntities() {
    return this.detectedEntities;
  }

  setTextFeedback(input) {
    this.textFeedback = input;
    const url = environment.apiUrl + '/feedback/analyze/ephc';
  }

  setVoiceFeedback(input) {
    this.voiceFeedback = input;
    const url = environment.apiUrl + '/feedback/analyze/ephc';
    // console.log(url)
  }
}
