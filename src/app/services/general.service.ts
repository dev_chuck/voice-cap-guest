import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  url;
  constructor(private http: HttpClient) { }

  SearchPlaces(data) {
    this.url = environment.apiUrl + '/assistant/places';

    const headers = new HttpHeaders().set('Content-Type', 'undefined');
    console.log('api hit');
    console.log(this.url);
    return this.http.post(this.url, data, { headers });
  }
}
