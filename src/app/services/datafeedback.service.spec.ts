import { TestBed } from '@angular/core/testing';

import { DatafeedbackService } from '@src/app/services/datafeedback.service';

describe('DatafeedbackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatafeedbackService = TestBed.get(DatafeedbackService);
    expect(service).toBeTruthy();
  });
});
