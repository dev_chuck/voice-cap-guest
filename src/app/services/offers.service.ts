import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class OffersService {
  url;
  constructor(private http: HttpClient) { }



  getOffersData() {
    this.url = 'assets/resources/offers/offersData.json';

    return this.http.get(this.url);
  }


  // getOffersData(){
  //   this.url = 'assets/resources/offers/offersData.json';


  //   return this.http.get(this.url);
  // }

  getOfferData() {
    this.url = 'assets/resources/offers/offer.json';

    return this.http.get(this.url);
  }
}


// `assets/resources/activities/food/food-${language}.json`
