import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'tns-core-modules/ui/page/page';
import { forkJoin } from 'rxjs'; // RxJS 6 syntax

// const async = require('async');
// const request = require('request');

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  url;
  num = 0;
  constructor(private http: HttpClient) {}

  getActivitiesData(caterogry, language) {
    if (language == undefined || null) {
      language = 'en';
    }
    // this.url = `assets/resources/activities/${caterogry}/${caterogry}-${language}.json`;
    switch (caterogry) {
      case 'food':
        this.url = `assets/resources/activities/food/food-${language}.json`;
        break;
      case 'culture':
        this.url = `assets/resources/activities/culture/museums-${language}.json`;
        break;
      case 'nightlife':
        this.url = `assets/resources/activities/nightlife/nightlife-${language}.json`;
        break;
      case 'tours':
        this.url = `assets/resources/activities/tours/tours-${language}.json`;
        break;
      case 'other':
        this.url = `assets/resources/activities/other/other-${language}.json`;
        break;
      default:
        console.log('No such data exists!');
        break;
    }
    if (this.url != null) {
      return this.http.get(this.url);
    }
  }

  getALLHotelData(language) {
    const response1 = this.http.get(
      `assets/resources/activities/hotel/hotel-${language}.json`
    );
    const response2 = this.http.get(
      `assets/resources/activities/food/food-${language}.json`
    );
    const response3 = this.http.get(
      `assets/resources/activities/culture/museums-${language}.json`
    );

    return forkJoin([response1, response2, response3]);
  }
  //    loadAllActivities(language)
  //     function httpGet(url, callback) {

  //       const options = {
  //         url :  url,
  //         json : true
  //       };
  //       // request(options,
  //       //   function(err, res, body) {
  //       //     callback(err, body);
  //       //   }
  //       // );
  //     }
  //     const urls= [
  //      ,
  //       ,
  //       ,
  //     ];

  //     async.map(urls, httpGet, function (err, res){
  //       if (err) return console.log(err);
  //       console.log(res);
  //     });
  // }

  // var resourceFileHotel = './client/app/resources/activities/hotel/hotel-' + + '.json'
  // // var resourceFileWalking = './client/app/resources/activities/walking-routes/walking-' + $rootScope.lang + '.json'
  // var resourceFileFood = './client/app/resources/activities/food/food-' +  + '.json'
  // var resourceFileCulture = './client/app/resources/activities/culture/museums-' +  + '.json'
  // $scope.hotel = $http.get(resourceFileHotel)
  // $scope.activitiesCulture = $http.get(resourceFileCulture)
  // // $scope.activitiesWalking = $http.get(resourceFileWalking)
  // $scope.activitiesFood = $http.get(resourceFileFood)
  // $q.all([$scope.hotel, $scope.activitiesCulture, $scope.activitiesFood]).then(function(values) {
  //     $scope.locations = values[0].data.concat(values[1].data).concat(values[2].data);
  //     console.log($scope.locations)
  //     $scope.initialize();
  // });
}
