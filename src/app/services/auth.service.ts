import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { environment } from ;
const BackendUrl = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}
  login(user) {
    return this.http.post(BackendUrl + '/guests/guest/save', user);
  }

  getServiceItem() {
    // localStorage.getItem
    return this.http.get(
      '../../resources/service-information/service-info-en.json'
    );
  }
}
