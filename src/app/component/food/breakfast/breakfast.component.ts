import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FooterService } from '@src/app/services/footer.service';
import { AnalyticsService } from '@src/app/services/analytics.service';

@Component({
  selector: 'app-breakfast',
  templateUrl: './breakfast.component.html',
  styleUrls: ['./breakfast.component.scss']
})
export class BreakfastComponent implements OnInit {

  constructor(private location: Location, private nav: FooterService, private googeAnalytics: AnalyticsService) {
    nav.show();
  }

  goBack() {
    this.location.back();
  }
  callReception() {
    this.googeAnalytics.sendEvent('call Reception button clicked');
    location.href = 'tel:0206642211';
  }


  ngOnInit() {
  }

}
