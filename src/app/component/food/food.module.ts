import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@src/app/material/material.module';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreakfastComponent } from '@src/app/component/food/breakfast/breakfast.component';
import { DinnerComponent } from '@src/app/component/food/dinner/dinner.component';
import { LunchComponent } from '@src/app/component/food/lunch/lunch.component';
import { RoomserviceComponent } from '@src/app/component/food/roomservice/roomservice.component';
import { FoodComponent } from '@src/app/component/food/food/food.component';
import { FoodRoutingModule } from '@src/app/component/food/food/food-routing.module';

// export const LoginRoutes: Routes = [
//   { path: "", component: FoodComponent },
//   { path: "breakfast", component : BreakfastComponent },
//   { path: "dinner", component : DinnerComponent },
//   { path: "lunch", component : LunchComponent },
//   { path: "roomservice", component: RoomserviceComponent }

// ];




@NgModule({
  declarations: [FoodComponent,
    BreakfastComponent,
    DinnerComponent,
    LunchComponent,
    RoomserviceComponent


  ],
  imports: [
    CommonModule,
    FoodRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule

  ]
})
export class FoodModule { }
