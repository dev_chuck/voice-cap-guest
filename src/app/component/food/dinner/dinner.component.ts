import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FooterService } from '@src/app/services/footer.service';

@Component({
  selector: 'app-dinner',
  templateUrl: './dinner.component.html',
  styleUrls: ['./dinner.component.scss']
})
export class DinnerComponent implements OnInit {

  constructor( private location: Location , private nav: FooterService) {
    nav.show();
   }

  goBack() {
    this.location.back();
  }


  callReception() {
    location.href = 'tel:0206642211';
  }

  ngOnInit() {
  }

}
