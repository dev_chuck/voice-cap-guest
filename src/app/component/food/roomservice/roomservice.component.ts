import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FooterService } from '@src/app/services/footer.service';



@Component({
  selector: 'app-roomservice',
  templateUrl: './roomservice.component.html',
  styleUrls: ['./roomservice.component.scss']
})
export class RoomserviceComponent implements OnInit {

  constructor( private location: Location , private nav: FooterService) {
    nav.show();
   }

  callReception() {
    location.href = 'tel:0206642211';
  }

  goBack() {
    // alert('back');
    this.location.back();
  }

  ngOnInit() {
  }

}
