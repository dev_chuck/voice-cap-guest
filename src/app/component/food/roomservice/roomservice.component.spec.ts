import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomserviceComponent } from '@src/app/roomservice/roomservice.component';

describe('RoomserviceComponent', () => {
  let component: RoomserviceComponent;
  let fixture: ComponentFixture<RoomserviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomserviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomserviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
