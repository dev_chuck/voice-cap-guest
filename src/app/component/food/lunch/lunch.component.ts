import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FooterService } from '@src/app/services/footer.service';

@Component({
  selector: 'app-lunch',
  templateUrl: './lunch.component.html',
  styleUrls: ['./lunch.component.scss']
})
export class LunchComponent implements OnInit {

  constructor( private location: Location, private nav: FooterService ) {
    nav.show();
   }

  goBack() {
    this.location.back();
  }

  ngOnInit() {
  }

}
