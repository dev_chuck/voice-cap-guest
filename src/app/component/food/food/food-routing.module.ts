
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodComponent } from '@src/app/component/food/food/food.component';
import { BreakfastComponent } from '@src/app/component/food/breakfast/breakfast.component';
import { DinnerComponent } from '@src/app/component/food/dinner/dinner.component';
import { RoomserviceComponent } from '@src/app/component/food/roomservice/roomservice.component';
import { LunchComponent } from '@src/app/component/food/lunch/lunch.component';

const routes: Routes = [
  { path: '', component: FoodComponent },
  { path: 'breakfast', component : BreakfastComponent },
  { path: 'dinner', component : DinnerComponent },
  { path: 'lunch', component : LunchComponent },
  { path: 'roomservice', component: RoomserviceComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodRoutingModule { }
