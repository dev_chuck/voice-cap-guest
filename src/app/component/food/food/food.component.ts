import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FooterService } from '@src/app/services/footer.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {

  constructor( private router: Router, private nav: FooterService ) {
    nav.show();
   }

  ngOnInit() {

  }

}
