import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from '@src/app/component/Map/map/map.component';


const routes: Routes = [
  {path: '', component: MapComponent},
  {
    path: ':category', component: MapComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapRoutingModule { }
