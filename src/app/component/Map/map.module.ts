import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderService } from '@src/app/services/loader.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import {LoaderInterceptor} from './../../services/loader.interceptor';
import { MapRoutingModule } from '@src/app/component/Map/map-routing.module';
import { MapComponent } from '@src/app/component/Map/map/map.component';
import { AgmCoreModule } from '@agm/core';
// import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';


@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyByt1VrvLsue5p08e7DJLrm1SXNGXaaCBc'
    }),
    MapRoutingModule,
    // AgmSnazzyInfoWindowModule
  ],

})
export class MapModule { }
