
import { AfterViewInit, Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { ActivityService } from '@src/app/services/activity.service';
import { ActivatedRoute } from '@angular/router';
import { FooterService } from '@src/app/services/footer.service';
import { AnalyticsService } from '@src/app/services/analytics.service';

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"]
})
export class MapComponent implements OnInit {
  // @ViewChild('mapRef', {static: true }) mapElement: ElementRef;
  // lat
  // lng
  serviceItems;
  catergory;
  id;
  language;
  testing;
  selectedPlaces;

  center;
  Hotels = [
    {
      location: { lat: 52.3839, lng: 4.9022 },
      name: "Sir Adam Hotel",
      city: "Amsterdam, The Netherlands",
      website: "https://www.sirhotels.com/en/adam/",
      information: `Set in a landmark skyscraper overlooking the IJ River, this funky boutique hotel is next to the EYE Film Museum, 
        a 2-minute walk from the Veer Buiksloterweg Ferry Terminal and 2 km from Dam Square.',
      image: 'assets/resources/images/sir-adam.png`
    }
  ];
  // abc = []
  responseData1;
  // responseData2
  // responseData3
  isOpen = true;
  zoom = 12;
  styles: any[] = [
    { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
    { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
    { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
    {
      featureType: "administrative.locality",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }]
    },
    {
      featureType: "poi",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }]
    },
    {
      featureType: "poi.park",
      elementType: "geometry",
      stylers: [{ color: "#263c3f" }]
    },
    {
      featureType: "poi.park",
      elementType: "labels.text.fill",
      stylers: [{ color: "#6b9a76" }]
    },
    {
      featureType: "road",
      elementType: "geometry",
      stylers: [{ color: "#38414e" }]
    },
    {
      featureType: "road",
      elementType: "geometry.stroke",
      stylers: [{ color: "#212a37" }]
    },
    {
      featureType: "road",
      elementType: "labels.text.fill",
      stylers: [{ color: "#9ca5b3" }]
    },
    {
      featureType: "road.highway",
      elementType: "geometry",
      stylers: [{ color: "#746855" }]
    },
    {
      featureType: "road.highway",
      elementType: "geometry.stroke",
      stylers: [{ color: "#1f2835" }]
    },
    {
      featureType: "road.highway",
      elementType: "labels.text.fill",
      stylers: [{ color: "#f3d19c" }]
    },
    {
      featureType: "transit",
      elementType: "geometry",
      stylers: [{ color: "#2f3948" }]
    },
    {
      featureType: "transit.station",
      elementType: "labels.text.fill",
      stylers: [{ color: "#d59563" }]
    },
    {
      featureType: "water",
      elementType: "geometry",
      stylers: [{ color: "#17263c" }]
    },
    {
      featureType: "water",
      elementType: "labels.text.fill",
      stylers: [{ color: "#515c6d" }]
    },
    {
      featureType: "water",
      elementType: "labels.text.stroke",
      stylers: [{ color: "#17263c" }]
    }

  ]
  constructor(private activity: ActivityService, private activeroute: ActivatedRoute, private nav: FooterService, private analyticsService : AnalyticsService ) {

    this.nav.show();
  }
  ngOnInit() {


    this.analyticsService.sendEvent('info-map-button-clicked');
    this.catergory = this.activeroute.snapshot.params.category;
    console.log("parmas id");

    var a = this.activeroute.params.subscribe(params => console.log(params['id']));



    console.log(a)
    console.log("caterogry")
    console.log(this.catergory)
    this.id = this.activeroute.snapshot.queryParamMap.get('id');;
    console.log("id")
    console.log(this.id)

    if (this.id) {
      this.Hotels = [];
    }

    this.language = localStorage.getItem("language");

    if (this.catergory != null || undefined) {
      // set the zoom level
      this.zoom = 11;
      this.loadActivityDataFromResources(this.catergory);
    } else {
      // set the zoom level
      this.zoom = 12;
      this.isOpen = false;
      //  this.setCurrentLocation()
      this.Hotels = [];
      this.activity.getALLHotelData(this.language).subscribe(Response => {
        this.isOpen = false;

        //  Response.forEach(item => {
        //   this.abc.push(item)

        // })
        this.responseData1 = Response[0];
        this.Hotels = this.responseData1
          .concat(Response[1])
          .concat(Response[2]);
        // this.Hotels.push(this.responseData1.concat(Response[1]).concat(Response[2]))
        console.log(this.Hotels);
        console.log("hello bhai");

        // Response.forEach(item => {
        //   this.abc.push(item)

        // })
        // console.log(this.abc)

        // this.abc.forEach(item => {
        //   this.Hotels.concat(item);
        // })
        console.log("hotels data");
        console.log(this.Hotels);
      });
    }

    // this.activity.getActivitiesData('food', this.language).subscribe(data => {
    //   this.serviceItems = data;
    //   console.log(this.serviceItems)
    // });
    //  this.setCurrentLocation()
  }

  loadActivityDataFromResources(category) {
    this.activity.getActivitiesData(category, this.language).subscribe(data => {
      this.serviceItems = data;
      this.serviceItems.forEach(item => {
        if (item.id == this.id) {
          this.center = item.location;
          this.selectedPlaces = item;
        }
      });
      console.log(this.center);
      console.log(this.selectedPlaces);
      this.Hotels.push(this.selectedPlaces);
      console.log(this.Hotels);
    });
  }

  async setCurrentLocation() {
    if ("geolocation" in navigator) {
      await navigator.geolocation.getCurrentPosition(position => {
        this.center = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
      });
    }
  }
}

// this.renderMap();

// loadMap = () => {
//   var map = new window['google'].maps.Map(this.mapElement.nativeElement, {
//     center: {lat: 24.5373, lng: 81.3042},
//     zoom: 8
//   });

//   var marker = new window['google'].maps.Marker({
//     position: {lat: 24.5373, lng: 81.3042},
//     map: map,
//     title: 'Hello World!',
//     draggable: true,
//     animation: window['google'].maps.Animation.DROP,
//   });

//   var contentString = '<div id="content">'+
//   '<div id="siteNotice">'+
//   '</div>'+
//   '<h3 id="thirdHeading" class="thirdHeading">Testing</h3>'+
//   '<div id="bodyContent">'+
//   '<p>yo man</p>'+
//   '</div>'+
//   '</div>';

//   var infowindow = new window['google'].maps.InfoWindow({
//     content: contentString
//   });

//     marker.addListener('click', function() {
//       infowindow.open(map, marker);
//     });

// }
// renderMap() {

//   window['initMap'] = () => {
//     this.loadMap();
//   }
//   if(!window.document.getElementById('google-map-script')) {
//     var s = window.document.createElement("script");
//     s.id = "google-map-script";
//     s.type = "text/javascript";
//     s.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyByt1VrvLsue5p08e7DJLrm1SXNGXaaCBc&callback=initMap";

//     window.document.body.appendChild(s);
//   } else {
//     this.loadMap();
//   }
// }

// //   hotel =  {
// //     location: {lat: 52.3839, lng: 4.9022},
// //     name: "Sir Adam Hotel",
// //     city: "Amsterdam, The Netherlands",
// //     website: "https://www.sirhotels.com/en/adam/",
// //     information: "Set in a landmark skyscraper overlooking the IJ River, this funky boutique hotel is next to the EYE Film Museum, a 2-minute walk from the Veer Buiksloterweg Ferry Terminal and 2 km from Dam Square.",
// //     image: '../../../../assets/adam.png'

// //     // "./client/app/resources/images/sir-adam.png"
// // }

//   constructor() { }

//   ngOnInit() {
//     // console.log(this.location)

//    this.setCurrentLocation();

//   }

//   addMarker(lat: number, lng: number) {
//    console.log(lat + '' + lng)
//   }
//  async setCurrentLocation(){
//     if ('geolocation' in navigator) {
//     await  navigator.geolocation.getCurrentPosition((position) => {

//           this.latitude = position.coords.latitude,
//         this.longitude  = position.coords.longitude

//       });
//     }
//   }

//   //  initMap() {
//   //   var uluru = {lat: -25.363, lng: 131.044};
//   //   var map = new google.maps.Map(document.getElementById('map'), {
//   //     zoom: 4,
//   //     center: uluru
//   //   });

//   //   var contentString = '<div id="content">'+
//   //       '<div id="siteNotice">'+
//   //       '</div>'+
//   //       '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
//   //       '<div id="bodyContent">'+
//   //       '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
//   //       'sandstone rock formation in the southern part of the '+
//   //       'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
//   //       'south west of the nearest large town, Alice Springs; 450&#160;km '+
//   //       '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
//   //       'features of the Uluru - Kata Tjuta National Park. Uluru is '+
//   //       'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
//   //       'Aboriginal people of the area. It has many springs, waterholes, '+
//   //       'rock caves and ancient paintings. Uluru is listed as a World '+
//   //       'Heritage Site.</p>'+
//   //       '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
//   //       'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
//   //       '(last visited June 22, 2009).</p>'+
//   //       '</div>'+
//   //       '</div>';

//   //   var infowindow = new google.maps.InfoWindow({
//   //     content: contentString
//   //   });

//   //   var marker = new google.maps.Marker({
//   //     position: uluru,
//   //     map: map,
//   //     title: 'Uluru (Ayers Rock)'
//   //   });
//   //   marker.addListener('click', function() {
//   //     infowindow.open(map, marker);
//   //   });
//   // }

// }
