import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { MapRoutingModule } from '@src/app/component/Map/map-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { MapComponent } from './map/map.component';


@NgModule({
  declarations: [MapComponent],
  imports: [
    MapRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MapModule { }
