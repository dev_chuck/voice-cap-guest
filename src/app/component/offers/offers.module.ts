import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersComponent } from '@src/app/component/offers/offers.component';
import { OffersRoutingModule } from '@src/app/component/offers/offers-routing.module';
import { OfferComponent } from '@src/app/component/offers/offer/offer.component';


@NgModule({
  declarations: [OffersComponent, OfferComponent],
  imports: [
    CommonModule,
    OffersRoutingModule
  ]
})
export class OffersModule { }
