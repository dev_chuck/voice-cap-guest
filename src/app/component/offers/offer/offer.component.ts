import { Component, OnInit } from '@angular/core';
import { OffersService } from '@src/app/services/offers.service';
// import { Observable } from 'tns-core-modules/ui/page/page';
import { ActivatedRoute } from '@angular/router';
import { FooterService } from '@src/app/services/footer.service';
import { AnalyticsService } from '@src/app/services/analytics.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit {
  offerData: [];
  offerHeader;

  constructor(private offer: OffersService, private activateroute: ActivatedRoute, private nav: FooterService, private analyticService : AnalyticsService) { 
    nav.show();
  }

  ngOnInit() {
    this.offerHeader = this.activateroute.snapshot.params.offer;
    this.offer.getOfferData().subscribe((data: any) => {
      this.offerData = data;

      console.log(this.offerData);
      
    })


  }
  order(offer) {
    console.log('order clicked');
    console.log(offer)
    this.analyticService.sendEvent('offer-' + offer + '-button-clicked')


    };


  
  
}
