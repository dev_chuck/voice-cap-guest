import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferComponent } from '@src/app/component/offers/offer/offer.component';

describe('OfferComponent', () => {
  let component: OfferComponent;
  let fixture: ComponentFixture<OfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
