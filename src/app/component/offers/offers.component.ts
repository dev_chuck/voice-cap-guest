import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OffersService } from '@src/app/services/offers.service';
import { FooterService } from '@src/app/services/footer.service';
import { AnalyticsService } from '@src/app/services/analytics.service';
// import * as Offerdata  from './../../../resources/offers/offersData.json'
// import * as Offer from './../../../resources/offers/offer.json'

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  OffersData: [] ;
  constructor(private router: Router, private offers: OffersService , private nav: FooterService, private analyticsService: AnalyticsService ) {
    nav.show();
  }

  ngOnInit() {

    this.offers.getOffersData().subscribe((data: any) => {
      this.OffersData   = data;



    });




  }

  offer(id) {

    if (id === 'offer1') {
      this.analyticsService.sendEvent('offers-' + id + '-see-more-button-clicked');

      this.router.navigate(['/offers', id]);
     } else {
       console.log('order clicked');
     }

  }

}
