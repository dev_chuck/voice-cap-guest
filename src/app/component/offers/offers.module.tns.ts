import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { OffersRoutingModule } from '@src/app/component/offers-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { OffersComponent } from '@src/app/component/offers/offers.component';
import { OfferComponent } from '@src/app/component/offers/offer/offer.component';


@NgModule({
  declarations: [OffersComponent, OfferComponent],
  imports: [
    OffersRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class OffersModule { }
