import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OffersComponent } from '@src/app/component/offers/offers.component';
import { OfferComponent } from '@src/app/component/offers/offer/offer.component';


const routes: Routes = [
  { path: '', component: OffersComponent },
  {
    path: ':offer', component: OfferComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
