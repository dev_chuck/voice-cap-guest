import { Component, OnInit } from '@angular/core';
import { FooterService } from '@src/app/services/footer.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private nav: FooterService, private translate: TranslateService) {
    const lang = localStorage.getItem('language');

    // this.useLanguage(lang);
    this.translate.use(lang);

    this.nav.show();
  }

  ngOnInit() {}

  //   useLanguage(language: string) {
  //     this.translateService.use(language);
  // }
}
