import { ActivitiessRoutingModule } from '@src/app/component/activities/activities/activities-routing.module.tns';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { CultureComponent } from '@src/app/component/activities/activities/culture/culture.component';
import { AssistantComponent } from '@src/app/component/activities/activities/assistant/assistant.component';
import { ActivityCategoryComponent } from '@src/app/component/activities/activities/activity-category/activity-category.component';


@NgModule({
  declarations: [ CultureComponent, AssistantComponent, ActivityCategoryComponent, ],
  imports: [
    ActivitiessRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ActivitiessModule { }
