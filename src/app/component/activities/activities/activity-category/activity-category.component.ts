import { Component, OnInit, ViewChild } from '@angular/core';
import { FooterService } from '@src/app/services/footer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbSlideEvent, NgbSlideEventSource, NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

import { ActivityService } from '@src/app/services/activity.service.js';

@Component({
  selector: 'app-activity-category',
  templateUrl: './activity-category.component.html',
  styleUrls: ['./activity-category.component.scss']
})
export class ActivityCategoryComponent implements OnInit {
  @ViewChild('mycarousel', { static: true }) carousel: NgbCarousel;
  categoryName: any;
  serviceItems: any;
  constructor(private nav: FooterService, private route: ActivatedRoute, private acitv: ActivityService, private router: Router) {
    nav.show();
  }


  ngOnInit() {
    this.categoryName = this.route.snapshot.params.name;
    console.log(this.categoryName);
    const lang = localStorage.getItem('language');

    this.acitv.getActivitiesData(this.categoryName, lang).subscribe(data => {
      this.serviceItems = data;
      console.log(this.serviceItems);
    });
  }
  onSlide(slideEvent: NgbSlideEvent) {
    console.log(slideEvent.source);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(slideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }
  mapCAll(cat, Id) {
    console.log(cat + ' ' + Id);
    this.router.navigate([`map/${cat}`], { queryParams: { id: Id } });

  }
}
