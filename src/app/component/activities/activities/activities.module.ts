import { ActivitiesComponent } from '@src/app/component/activities/activities/activities.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitiessRoutingModule } from '@src/app/component/activities/activities/activities-routing.module';
import {LoaderInterceptor} from '@src/app/services/loader.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AssistantComponent } from '@src/app/component/activities/activities/assistant/assistant.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActivityCategoryComponent } from '@src/app/component/activities/activities/activity-category/activity-category.component';
import { MaterialModule } from '@src/app/material/material.module';


@NgModule({
  declarations: [ActivitiesComponent, AssistantComponent, ActivityCategoryComponent],
  imports: [
    CommonModule,
    ActivitiessRoutingModule,
    NgbModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: LoaderInterceptor ,
    multi: true
  }]
})
export class ActivitiessModule { }
