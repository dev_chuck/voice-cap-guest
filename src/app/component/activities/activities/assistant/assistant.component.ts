import { Component, OnInit } from '@angular/core';
import { FooterService } from '@src/app/services/footer.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AudioRecordingService } from '@src/app/services/audio-recording.service';
// import { ActivitesService } from '../activites.service';
import { Router } from '@angular/router';
import { GeneralService } from '@src/app/services/general.service';
import { LoaderService } from '@src/app/services/loader.service';
import { interval } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-assistant',
  templateUrl: './assistant.component.html',
  styleUrls: ['./assistant.component.scss']
})
export class AssistantComponent implements OnInit {
  setInterval = setInterval;

  isRecording = false;
  recordedTime;
  blobUrl;
  blob;
  places;
  AudioMikeVisible = true;
  analzingFeedbackVisible = false;
  recordingStart = false;
  analyzingbox = false;
  error = false;
  showSpinner = false;
  x;
  constructor(
    private loader: LoaderService,
    private router: Router,
    private audioRecordingService: AudioRecordingService,
    private sanitizer: DomSanitizer,
    private nav: FooterService,
    private activitesService: GeneralService
  ) {
    nav.show();
  }

  ngOnInit() {
    this.error = false;
    this.audioRecordingService.recordingFailed().subscribe(() => {
      this.isRecording = false;
    });

    this.audioRecordingService.getRecordedTime().subscribe(time => {
      this.recordedTime = time;
    });

    this.audioRecordingService.getRecordedBlob().subscribe(data => {
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(
        URL.createObjectURL(data)
      );
    });
  }

  startRecording() {
    this.error = false;
    this.AudioMikeVisible = false;

    this.recordingStart = true;

    if (!this.isRecording) {
      this.isRecording = true;
      this.audioRecordingService.startRecording();
    }
  }

  abortRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      this.audioRecordingService.abortRecording();
    }
  }

  stopRecording() {
    this.error = false;
    this.recordingStart = false;

    this.analyzingbox = true;
    if (this.isRecording) {
      this.audioRecordingService.stopRecording();

      this.isRecording = false;
      this.analyzeVoiceFeedback();
    }
  }

  clearRecordedData() {
    clearInterval(this.x);
    console.log(this.blobUrl);
    this.blobUrl = null;
  }

  ngOnDestroy(): void {
    console.log(this.blobUrl);

    this.abortRecording();
  }

  analyzeVoiceFeedback() {
    this.showSpinner = true;
    this.analzingFeedbackVisible = true;
    this.audioRecordingService.getRecordedBlob().subscribe(data => {
      if (data) {
        this.blob = data;
      }
      console.log('reocded blob in anlayze voice'), console.log(data);
    });
    this.sendFeedbackToBackendToExtractConcepts(this.blob);
  }

  sendFeedbackToBackendToExtractConcepts(blob) {
    console.log('blob sending to server');
    console.log(blob);
    const payload = new FormData();
    payload.append('feedbackAudioFile', blob);
    const lang = localStorage.getItem('language');
    console.log('lang');
    payload.append('language', lang);
    console.log('blob url');
    // console.log(this.blobUrl);

    console.log(payload);
    this.activitesService.SearchPlaces(payload).subscribe(responseData => {
      this.showSpinner = false;

      this.places = responseData;
      console.log(responseData);
      this.places = responseData;
      if (this.places.length > 0) {
        this.router.navigate(['places']);
      } else {
        this.error = true;
        this.analyzingbox = false;
        // const seconds = interval(1000)
        // seconds.pipe(timeout(1100)).subscribe(
        //   value => this.navigateAssistant(),
        //   err => console.log(err),     // Will emit error before even first value is emitted,

        // );
        // this.setIntrvl()

        this.x = setTimeout(() => {
          console.log('timeout!');
          this.x = null;
          this.stopRecording();
          this.abortRecording();

          this.audioRecordingService.stopMedia();
          this.analyzingbox = false;
          this.error = false;

          this.AudioMikeVisible = true;
        }, 3000);
        // this.error = false;
        // if(this.x){
        //   this.error = false;
        //   this.AudioMikeVisible = true;

        //   clearInterval(this.x);

        // }
      }
    });
    this.recordedTime = '00:00';

    this.clearRecordedData();
  }

  // navigateAssistant(){
  //   // this.router.navigate(['activities/Assistant'])
  //   this.router.navigate(['/activities/Assistant']);

  // }
  // setIntrvl(){
  //   setInterval(() => this.navigateAssistant() ,1000);
  // }
}
