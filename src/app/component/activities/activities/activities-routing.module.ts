import { ActivitiesComponent } from '@src/app/component/activities/activities/activities.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { WalkingRoutesComponent } from './walking-routes/walking-routes.component';

import { AssistantComponent } from '@src/app/component/activities/activities/assistant/assistant.component';
import { ActivityCategoryComponent } from '@src/app/component/activities/activities/activity-category/activity-category.component';


const routes: Routes = [
  { path: '', component: ActivitiesComponent },

  {
    path: 'Assistant', component: AssistantComponent, pathMatch: 'full'
  },
  {
    path: ':name', component : ActivityCategoryComponent, pathMatch: 'full'
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivitiessRoutingModule { }
