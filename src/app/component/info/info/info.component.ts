import { Component, OnInit } from '@angular/core';
import { FooterService } from '@src/app/services/footer.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  constructor(private nav: FooterService) { this.nav.show(); }

  ngOnInit() {
  }

}
