
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@src/app/material/material.module';
import { InfoComponent } from '@src/app/component/info/info/info.component';
import { RouterModule, Routes } from '@angular/router';
export const LoginRoutes: Routes = [
  { path: '', component: InfoComponent }
];


@NgModule({
  declarations: [InfoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    MaterialModule
  ]
})
export class InfoModule { }
