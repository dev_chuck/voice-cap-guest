import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { PlacesRoutingModule } from '@src/app/component/places/places-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { PlacesComponent } from '@src/app/component/places/places/places.component';


@NgModule({
  declarations: [PlacesComponent],
  imports: [
    PlacesRoutingModule,
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PlacesModule { }
