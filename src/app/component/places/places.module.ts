import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlacesRoutingModule } from '@src/app/component/places/places-routing.module';
import { PlacesComponent } from '@src/app/component/places/places/places.component';


@NgModule({
  declarations: [PlacesComponent],
  imports: [
    CommonModule,
    PlacesRoutingModule
  ]
})
export class PlacesModule { }
