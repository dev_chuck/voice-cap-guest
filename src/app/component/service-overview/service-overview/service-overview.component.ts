import { Component, OnInit } from '@angular/core';
import { ServiceComponent } from '@src/app/component/service-overview/service/service.component';
import { FooterService } from '@src/app/services/footer.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-service-overview',
  templateUrl: './service-overview.component.html',
  styleUrls: ['./service-overview.component.scss']
})
export class ServiceOverviewComponent extends ServiceComponent implements OnInit {
  // localItem: { name: string, desription: string }
  name: string;
  constructor(nav: FooterService, private route: ActivatedRoute) {
    super(nav);
    nav.show();
  }

  ngOnInit() {
    this.name = this.route.snapshot.paramMap.get('id');
    super.getData(this.name);
    console.log(this.getObject, 'burrrr');
  }

}
