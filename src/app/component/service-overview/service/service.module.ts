import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@src/app/material/material.module';
import { ServiceComponent } from '@src/app/component/service-overview/service/service.component';
import { RouterModule, Routes } from '@angular/router';
import { ServiceOverviewComponent } from '@src/app/component/service-overview/service-overview/service-overview.component';
export const LoginRoutes: Routes = [
  { path: '', component: ServiceComponent },
  { path: 'service-overview/:id', component: ServiceOverviewComponent }
];


@NgModule({
  declarations: [ServiceComponent, ServiceOverviewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    MaterialModule
  ]
})
export class ServiceModule { }
