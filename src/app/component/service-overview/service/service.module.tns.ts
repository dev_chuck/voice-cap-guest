import { ServiceComponent } from '@src/app/component/service-overview/service/service.component';


import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { ServiceOverviewComponent } from '@src/app/component/service-overview/service-overview/service-overview.component';



@NgModule({
  declarations: [ServiceComponent, ServiceOverviewComponent],
  imports: [
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ServiceModule { }
