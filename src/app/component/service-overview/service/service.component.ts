import { FooterService } from "@src/app/services/footer.service";
import { Component, OnInit } from "@angular/core";
import * as data from "@src/app/resources/service-information/service-info-en.json";
import * as nederland from "@src/app/resources/service-information/service-info-nl.json";

@Component({
  selector: "app-service",
  templateUrl: "./service.component.html",
  styleUrls: ["./service.component.scss"]
})
export class ServiceComponent implements OnInit {
  public serviceItems: { name: string; desription: string }[];
  filteredArray = [];
  public getObject: { name: string; desription: string };
  constructor(private nav: FooterService) {
    this.nav.show();
  }

  ngOnInit() {
    console.log(data);
    const lang = localStorage.getItem("language");
    if (lang == "en") {
      this.serviceItems = (data as any).default;
      console.log(this.serviceItems, "service");
    } else if (lang == "nl") {
      this.serviceItems = (nederland as any).default;
      console.log(this.serviceItems, "service nl");
    }
  }
  filter(character) {
    this.serviceItems = (data as any).default;
    this.filteredArray = [];
    for (let i = 0; i < this.serviceItems.length; i++) {
      const name = this.serviceItems[i].name;
      console.log(name.charAt(0));
      if (name.charAt(0) == character) {
        this.filteredArray.push(this.serviceItems[i]);
      }
    }
    console.log(this.filteredArray);
    if (this.filteredArray.length != 0) {
      this.serviceItems = this.filteredArray;
    } else {
      // this.serviceItems = (data as any).default;
      this.serviceItems = [];
    }
  }
  getData(name) {
    this.serviceItems = (data as any).default;
    for (let j = 0; j < this.serviceItems.length; j++) {
      if (this.serviceItems[j].name == name) {
        this.getObject = this.serviceItems[j];
      }
    }
  }
}
