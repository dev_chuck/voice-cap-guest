import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { FeedbackComponent } from '@src/app/component/feedback/feedback/feedback.component';
import { RatingComponent } from '@src/app/component/feedback/rating/rating.component';



@NgModule({
  declarations: [FeedbackComponent, RatingComponent],
  imports: [
    NativeScriptCommonModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class FeedbackModule { }
