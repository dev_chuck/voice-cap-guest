import { AudioRecordingService } from '@src/app/services/audio-recording.service';
import { AnalyticsService } from '@src/app/services/analytics.service';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Route, Router } from '@angular/router';
import * as WaveSurfer from 'wavesurfer.js';
import MicrophonePlugin from 'wavesurfer.js/dist/plugin/wavesurfer.microphone.min.js';
import { DatafeedbackService } from '@src/app/services/datafeedback.service';
import { FooterService } from '@src/app/services/footer.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { empty } from 'rxjs';

// import { DataService } from '@src/app/services/data.service';
// import * as $ from 'jquery';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnDestroy {

  milan = false;
  isRecording = false;
  recordedTime;
  blobUrl;
  public show = true;
  public show1 = false;
  public feedback = false;
  recordedvoice;
  public rating = false;
  feeddata;
  ratingData;
  RatingVisible = false;
  wavesurfer: any;
  data: [];
  public score = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  toggle = null;
  toggleIndex = null;
  public thankyou = false;
  active = false;


  
  constructor(private audioRecordingService: AudioRecordingService, private sanitizer: DomSanitizer, private analyticsService : AnalyticsService, private feedbackService: DatafeedbackService, private footerservice: FooterService) { footerservice.show() }

  startRecording() {

    this.analyticsService.sendEvent('feedback-start-recording-microphone-button-clicked');
    //console.log('feedback-start-recording-microphone-button-clicked');

    if (!this.isRecording) {
      this.isRecording = true;
      this.audioRecordingService.startRecording();


      this.wavesurfer = WaveSurfer.create({
            container: '#waveform',
            msDisplayMax: 1,
            hideScrollbar: true,
            debug: true,
            fillParent : true,
            responsive : false,
            waveColor: 'black',
            progressColor: 'black',
            cursorColor: 'black',
            plugins: [MicrophonePlugin.create()]
          });

      this.wavesurfer.microphone.on('deviceReady', function(stream) {
        console.info('Device ready!', stream);
      });
      this.wavesurfer.microphone.on('deviceError', function(code) {
        console.warn('Device error: ' + code);
      });
      const microphone = this.wavesurfer.microphone;
      microphone.start();

    }
    }



  abortRecording() {
    if (this.isRecording) {
      this.isRecording = false;
      const microphone = this.wavesurfer.microphone;
      microphone.stopDevice();
      this.wavesurfer.destroy();

      this.audioRecordingService.abortRecording();
    }
  }

  stopRecording() {
    this.analyticsService.sendEvent('feedback-stop-recording-button-clicked');
    if (this.isRecording) {
        const microphone = this.wavesurfer.microphone;
        microphone.stopDevice();
        this.wavesurfer.destroy();
        this.audioRecordingService.stopRecording();
        this.isRecording = false;
    }
  }


  clearRecordedData() {


    console.log('clear data');
    this.blobUrl = null;
  }


  textfeedback() {
    this.show1 = true;
    this.show = false;
    if (this.show1 == true) {
      this.abortRecording();
    }

  }

  voicefeedback() {
    this.show = true;
    this.show1 = false;
  }

  analyzeTextFeedback() {
    this.analyticsService.sendEvent('feedback-analyze-text-feedback-button-clicked');
    this.show1 = false;
    this.feedback = false;
    this.sendfeedback();
    // this.RatingVisible = true;
    // this.show = false;
  }

  analyzeVoiceFeedback() {
    this.analyticsService.sendEvent('feedback-analyze-voice-feedback-button-clicked');
    this.show = false;
    this.feedback = true;
    this.sendfeedback();
    // this.RatingVisible = true;


  }

  getVoiceRecording() {
    this.recordedvoice = this.blobUrl;
    return this.recordedvoice;
  }



  giveRating(index,score){
    //alert(score);
    this.analyticsService.sendEvent('rating-score-given');
    this.toggle = score;
    this.toggleIndex = index;
    this.active = !this.active;
    
  }



  goToContactPage(){
    //this.sendfeedback();
    this.analyticsService.sendEvent('rating-send-ratings-button-clicked');
    this.RatingVisible = false;
    this.feedback = false;
    this.thankyou = true;
  }


  sendfeedback() {
    console.log('feedback');
    const payload = new FormData();

    if (this.getVoiceRecording() != null || this.getVoiceRecording() != undefined) {
      payload.append('feedbackAudioFile', this.blobUrl);
      // const mp3Name = encodeURIComponent('audio_' + new Date().getTime() + '.mp3');

      this.feedbackService.setVoiceFeedback(this.blobUrl);
    }


    payload.append('feedbackTypedText', 'de receptie en parkeren en lunch vielen erg tegen');
    this.feedbackService.setTextFeedback('de receptie en parkeren en lunch vielen erg tegen');

    payload.append('language', 'nl-NL');
    console.log(payload);
    console.log('file data url');
    console.log(this.blobUrl);
    this.feedbackService.sendfeedback(payload).subscribe( (response: any) => {
      if (response) {
        this.data = response;
        console.log(this.data);
        this.RatingVisible = true;
        this.show = false;
      }

    });

  }

  ngOnDestroy(): void {
    this.abortRecording();
  }

  ngOnInit() {
    this.audioRecordingService.recordingFailed().subscribe(() => {
      this.isRecording = false;
    });

    this.audioRecordingService.getRecordedTime().subscribe((time) => {
      this.recordedTime = time;
    });

    this.audioRecordingService.getRecordedBlob().subscribe((data) => {
      this.blobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data));
    });
  }
}












