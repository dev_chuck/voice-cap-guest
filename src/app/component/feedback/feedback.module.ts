import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackComponent } from '@src/app/component/feedback/feedback/feedback.component';
import { MaterialModule } from '@src/app/material/material.module';
import { RouterModule, Routes } from '@angular/router';
import { DatafeedbackService } from '@src/app/services/datafeedback.service';


export const LoginRoutes: Routes = [
  { path: '', component: FeedbackComponent, pathMatch: 'full' },
  { path: '**', redirectTo: 'feedback', pathMatch: 'full' },
  { path: '/feedback', redirectTo: 'feedback', pathMatch: 'full' }


];


@NgModule({
  declarations: [
    FeedbackComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(LoginRoutes),
    MaterialModule
  ],
  providers: [DatafeedbackService]
})
export class FeedbackModule { }


