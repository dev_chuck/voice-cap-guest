import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'voice';
  constructor(private router: Router) {
    const navEndEvents$ = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    );

    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('js', new Date()),
        gtag(
          'config',
          'UA-148598088-1',

          {
            page_path: event.urlAfterRedirects
          }
        );
    });
  }

  //   $rootScope.$on('$viewContentLoaded', function(event) {
  //     gtag('js', new Date());
  //     gtag('config', 'UA-148598088-1', {'page_path': $location.path()});
  // });
  ngOnInit() {
    if (localStorage.getItem('guestID') != undefined || null) {
      this.router.navigate(['/home']);
    } else {
      const name = localStorage.getItem('name');
      const email = localStorage.getItem('email');
      const guestId = localStorage.getItem('guestID');
      console.log(name, email, guestId);
      if (
        name == null ||
        name == undefined ||
        email == null ||
        email == undefined
      ) {
        console.log('no name');
        this.router.navigate(['/login']);
        return;
      } else {
        return true;
      }
    }
  }
}
